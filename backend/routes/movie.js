const { request, response } = require('express')
const db = require('../db')
const utils = require('../utils')
const express = require('express')
const router = express.Router()

router.post('/addStudent', (request, response) => {
    const { s_name, password, course ,passing_year, prn_no, dob } = request.body
    
    const query = 'insert into Students_tb ( s_name, password, course ,passing_year, prn_no, dob ) values(?,?,?,?,?,?)'
    db.pool.execute(query, [s_name, password, course ,passing_year, prn_no, dob], (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.get('/displayStudent', (request, response) => {
    const query = 'select s_name, password, course ,passing_year, prn_no, dob from Students_tb'
    db.pool.execute(query, (error, result) => {
        response.send(utils.createResult(error,result))
    })
})


module.exports = router
